<?php

class Building
{

    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address)
    {

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFloors()
    {
        return $this->floors;
    }

    public function getAddress()
    {
        return $this->address;
    }


    //setter (mutators)
    public function setName($name)
    {
        //$this->name = $name;
        if (strlen($name) !== 0) {
            $this->name = $name;
        }
    }
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building
{
    //Encapsulation indicates that data must not be directly accessible to users but through a public function (setter and getter)

    //getter(accessors)
    // public function getName()
    // {
    //     return $this->name;
    // }

    // public function getName()
    // {
    //     return $this->name;
    // }

    //setter (mutators)
    // public function setName($name)
    // {
    //     //$this->name = $name;
    //     if (strlen($name) !== 0) {
    //         $this->name = $name;
    //     }
    // }
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
